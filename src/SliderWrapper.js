import React, { useRef, useState, useEffect } from 'react';
import Slider from './Slider';

// const SliderWrapper = ({ gender }) => {
const SliderWrapper = () => {
    const chartArea = useRef(null);
    const [chart, setChart] = useState(null);

    useEffect(() => {
        if (!chart) {
            setChart(new Slider(chartArea.current));
        }
        // skip the loading state, when data is still a pending promise
        else if (chart.menData) {
            // chart.update(gender);
        }
    }, [chart]);
    // }, [chart, gender]);

    return <div className="chart-area" ref={chartArea}></div>;
};

export default SliderWrapper;
