// https://observablehq.com/@d3/brush-handles?collection=@d3/d3-brush
import * as d3 from 'd3';
// import rect from 'd3';

import rawGrilldata from './data/grill_data.json';
import dateParser from './dateParser';

export default class Slider {
    constructor(element) {
        const { eventsData } = dateParser(rawGrilldata);

        const margin = { top: 10, right: 0, bottom: 20, left: 0 };
        const width = 800;
        let height = 100;

        let x = d3
            .scaleTime()
            .domain(d3.extent(eventsData, (d) => d.dateToShow))
            .range([40, width - 10]);

        let xAxis = d3.axisBottom(x).tickFormat(d3.timeFormat('%Y'));

        let y = d3
            .scaleLinear()
            .domain(d3.extent(eventsData, (d) => d.events))
            .range([height - 20, 10]);

        let yAxis = d3.axisLeft(y);

        const svg = d3.select(element).append('svg').attr('viewBox', [0, 0, width, height]);

        let path = svg
            .append('path')
            .datum(eventsData)
            .attr('fill', 'none')
            .attr('stroke', 'steelblue')
            .attr('stroke-width', 1.5)
            .attr(
                'd',
                d3
                    .line()
                    .x((d) => {
                        return x(d.dateToShow);
                    })
                    .y((d) => {
                        return y(d.events);
                    })
            );

        const arc = d3
            .arc()
            .innerRadius(0)
            .outerRadius((height - margin.top - margin.bottom) / 2)
            .startAngle(0)
            .endAngle((d, i) => (i ? Math.PI : -Math.PI));

        const brushHandle = (g, selection) =>
            g
                .selectAll('.handle--custom')
                .data([{ type: 'w' }, { type: 'e' }])
                .join((enter) =>
                    enter
                        .append('path')
                        .attr('class', 'handle--custom')
                        .attr('fill', '#666')
                        .attr('fill-opacity', 0.8)
                        .attr('stroke', '#000')
                        .attr('stroke-width', 1.5)
                        .attr('cursor', 'ew-resize')
                        .attr('d', arc)
                )
                .attr('display', selection === null ? 'none' : null)
                .attr(
                    'transform',
                    selection === null
                        ? null
                        : (d, i) =>
                            `translate(${selection[i]},${
                                (height + margin.top - margin.bottom) / 2
                            })`
                );

        const brush = d3
            .brushX()
            .extent([
                [margin.left, margin.top],
                [width - margin.right, height - margin.bottom],
            ])

            .on('start brush end', brushed);

        svg.append('g')
            .attr('transform', `translate(0, ${height - 20})`)
            .call(xAxis);

        svg.append('g').attr('transform', 'translate(40, 0)').call(yAxis);

        svg.append('g').call(brush).call(brush.move, [0.3, 0.5].map(x));


        function brushed({ selection }) {
            path.attr();
            d3.select(this).call(brushHandle, selection);
        }

        return svg.node();
    }
}
