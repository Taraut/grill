/*
    "\n  I like ... though.\n",
    "Reviewed in the United States on September 11, 2020",
    -0.2,
    -0.2,
    20,
    "the United States",
    "Cuisinart-GR-300WS-Griddler-Elite-Stainless"
    */

import _ from 'underscore';
import eventsCounter from './eventsCounter';

const dateParser = (rawGrillData) => {
    let grillData = [];

    rawGrillData.forEach((element) => {
        let revDate = element[1].substring(element[1].indexOf(' on ') + 4);

        grillData.push({
            text: element[0], //  "\n  I like ... though.\n"
            reviewDate: new Date(revDate), //  "2020-12-31"
            size: element[4], //  20
        });
    });

    grillData = _.sortBy(grillData, 'reviewDate');

    const eventsData = eventsCounter(grillData);

    const parsedData = {
        grillData,
        eventsData,
    };

    return parsedData;
};

export default dateParser;
