const eventsCounter = (grillData) => {
    let eventsData = [
        {
            dateToShow: grillData[0].reviewDate,
            events: 1,
        },
    ];

    let arrayElement = 0;

    for (let index = 0; index < grillData.length - 1; index++) {
        let day1 = grillData[index].reviewDate;
        let day2 = grillData[index + 1].reviewDate;

        if (day1.getFullYear() === day2.getFullYear() && day1.getMonth() === day2.getMonth()) {
            eventsData[arrayElement].events++;
        }

        if (day1.getFullYear() != day2.getFullYear() || day1.getMonth() != day2.getMonth()) {
            arrayElement++;

            eventsData.push({
                dateToShow: grillData[index + 1].reviewDate,
                events: 1,
            });
        }
    }

    return eventsData;
};

export default eventsCounter;
