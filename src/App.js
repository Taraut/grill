import React from 'react';
import './App.css';

// import ChartWrapper from './chartWrapper';
import SliderWrapper from './SliderWrapper';

// import grillData from './data/grill_data.json';
// import dateParser from './dateParser';

function App() {
    // let processedGrillData = dateParser(grillData);

    // console.log(processedGrillData);

    return (
        <div className="App">
            <SliderWrapper></SliderWrapper>
        </div>
    );
}

export default App;
